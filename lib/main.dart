import 'package:flag/flag.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/net/openweather.dart';
import 'package:flutter_app/net/translate.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'entity/weather.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Météo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        backgroundColor: Colors.blueGrey,
        appBar: AppBar(
          title: Text('Météo'),
        ),
        body: Column(
          children: <Widget>[
            WeatherHeader(),
            Center(
              child: WeatherBody(),
            ),
          ],
        ),
      ),
    );
  }
}

class WeatherHeader extends StatefulWidget {
  @override
  _WeatherHeaderState createState() => _WeatherHeaderState();
}

class _WeatherHeaderState extends State<WeatherHeader> {
  String dropdownValue;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: DropdownButton<String>(
            items: <String>[
             'FR', 'DE', 'GB', 'ES'
            ]
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: CountryFlags(countryCode: value, width: 100, height: 100),
              );
            }).toList(),
            value: dropdownValue,
            onChanged: (String newValue) async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.setString('lang', newValue);
              setState(() {
                dropdownValue = newValue;
              });
            },
          ),
        ),
        Expanded(
          child: IconButton(
            icon: new Icon(Icons.search),
            tooltip: 'Search',
            onPressed: () => null,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}

class CountryFlags extends StatelessWidget {
  final double height;
  final double width;
  final String countryCode;


  const CountryFlags({ Key key , this.countryCode, this.width, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flags.getFullFlag( this.countryCode, this.height, this.width);
  }
}

class WeatherBody extends StatefulWidget {
  @override
  _WeatherBodyState createState() => _WeatherBodyState();
}

class _WeatherBodyState extends State<WeatherBody> {
  Weather weather;
  String date;

  @override
  void initState() {
    weather = null;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _getWeather();
    _getTime();
    if (weather != null) {
      return Container(
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(50.0),
          child: Column(
            children: <Widget>[
              Text('${weather.city}',
                  style: new TextStyle(color: Colors.white, fontSize: 32.0)),
              Text('${weather.weather}',
                  style: new TextStyle(color: Colors.white, fontSize: 42.0)),
              Text('${weather.temperature}°C',
                  style: new TextStyle(color: Colors.white, fontSize: 32.0)),
              Image.network(weather.icon, scale: 0.5),
              Text(date,
                  style: new TextStyle(color: Colors.white, fontSize: 20.0)),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: IconButton(
            icon: new Icon(Icons.refresh),
            tooltip: 'Refresh',
            onPressed: () => _refreshDataScreen(),
            color: Colors.white,
          ),
        )
      ]));
    } else {
      return Container(
        child: CupertinoActivityIndicator(),
      );
    }
  }

  Future<Position> _getPosition() async {
    return await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  _getWeather() async {
    Position p = await _getPosition();
    Weather w = await WeatherApi().fetchWeather(p);
    w = await _getTranslate(w);
    setState(() {
      weather = w;
    });
  }

  _getTime() {
    DateTime now = DateTime.now();
    String d = new DateFormat('dd-MM-yyyy HH:mm:ss').format(now);
    setState(() {
      date = d;
    });
  }

  _getTranslate(Weather weather) async {
    String trad = await TradApi().fetchTranslate(weather.weather);
    weather.weather = trad;
    return weather;
  }

  _refreshDataScreen() {
    _getWeather();
    _getTime();
  }
}
