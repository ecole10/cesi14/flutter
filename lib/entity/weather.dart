import 'package:weather_icons/weather_icons.dart';

class Weather {
  String city;
  String weather;
  String temperature;
  String humidity;
  String icon;

  Weather();

  Weather.fromJson(Map<String, dynamic> json) :
        city = json['name'],
        weather = json['weather'][0]['description'].toString(),
        temperature = json['main']['temp'].toString(),
        humidity = json['main']['humidity'].toString(),
        icon = 'https://openweathermap.org/img/w/'+json['weather'][0]['icon']+'.png';
}
