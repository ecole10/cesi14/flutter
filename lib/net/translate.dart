import 'package:flutter_app/config/config.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';

class TradApi {
  static final String apiHost = "translation.googleapis.com";
  static final String apiPath = "language/translate/v2/";
  static final TradApi instance = TradApi._internal();

  TradApi._internal();

  factory TradApi() {
    return instance;
  }

  Uri buildUri(String q) {
//    String lang = getLang();

    return Uri(
        scheme: "https",
        host: apiHost,
        path: "$apiPath/",
        queryParameters: {
          "target": 'fr',
          "key": AppConfig.apiKeyTranslate,
          "q": q,
        });
  }

  Future<String> fetchTranslate(String q) async {
    SharedPreferences pref = await SharedPreferences.getInstance();

    if (!pref.containsKey(q)) {
      Uri uri = buildUri(q);
      http.Response res = await http.get(uri.toString());

      if (res.statusCode == 200) {
        var translate = convert.jsonDecode(res.body);
        pref.setString(
            q, translate['data']['translations'][0]['translatedText']);
      }

      print("Request fail, status ${res.statusCode}");
      return null;
    }

    return pref.getString(q);
  }

//  String getLang() async {
//    Future<SharedPreferences> pref = SharedPreferences.getInstance();
//    String lang;
//
//    if(pref.containsKey('lang')) {
//      lang = pref.getString('lang');
//    } else {
//      lang = "fr";
//    }
//
//    return lang;
//  }
}
