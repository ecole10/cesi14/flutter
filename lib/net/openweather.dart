import 'package:flutter_app/config/config.dart';
import 'package:flutter_app/entity/weather.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class WeatherApi {
  static final String apiHost = "api.openweathermap.org";
  static final String apiPath = "data/2.5/";
  static final WeatherApi instance = WeatherApi._internal();

  WeatherApi._internal();

  factory WeatherApi() {
    return instance;
  }

  Uri buildUri(String type, Position pos) {
    return Uri(
        scheme: "https",
        host: apiHost,
        path: "$apiPath/$type",
        queryParameters: {
          "lat": pos.latitude.toString(),
          "lon": pos.longitude.toString(),
          "units": "metric",
          "apikey": AppConfig.apiKeyWeather,
        });
  }

  Future<Weather> fetchWeather(Position p) async {
    Uri uri = buildUri("weather", p);
    http.Response res = await http.get(uri.toString());

    if(res.statusCode == 200) {
      Map weatherMap = convert.jsonDecode(res.body);
      Weather weather = Weather.fromJson(weatherMap);
      return weather;
    }

    print("Request fail, status ${res.statusCode}");
    return null;
  }
}
